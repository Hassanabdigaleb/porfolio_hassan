import './App.css';
import Header from './components/Header';


import { useState } from 'react';
import Accueil from './pages/Accueil';
import ProjetDestop from './pages/ProjetDestop.jsx';
import ProjetWeb from './pages/ProjetWeb';
import Contact from './pages/Contact';


export default function App() {

  
  const [pageCourant ,setPageCourant]=useState("Accueil")

  const ChangePage =(page)=>{
    return ()=>{
      setPageCourant(page)

    }
    
  }


  return (
    <div className="App">
     <Header ChangePage={ChangePage}/>


     {pageCourant==="Accueil" && <Accueil nom="hassan" />}
      {pageCourant==="ProjetWeb" && <ProjetWeb nom="hassan" />}
      {pageCourant==="ProjetDestop" && <ProjetDestop nom="hassan" />}
      {pageCourant==="Contact" && <Contact nom="hassan" />}
      


        
    </div>
  );
}


