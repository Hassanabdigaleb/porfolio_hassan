import types from "./Nav.module.css"
export default function Nav(props) {
    return <nav className={types.Nav}>
        <ul>
            <li>
                <button onClick={props.ChangePage("Accueil")}> Accueil</button>
            </li>
            <li>
                <button onClick={props.ChangePage("ProjetWeb")}> Projet Web</button> 
            </li>
            <li>
                <button onClick={props.ChangePage("ProjetDestop")}> Projet Destop</button>
            </li>
            
            <li>
                <button onClick={props.ChangePage("Contact")}>  Contact</button>
            </li>
        </ul>
    </nav>
}
