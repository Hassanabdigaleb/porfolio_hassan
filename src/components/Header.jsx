import types from "./Header.module.css"
import Nav from "./Nav"
export default function Header(props) {
    return <header className={types.header}>
        <h1> Porfolio</h1>
        
        <div>
            bonjour, {props.nom } {props.prenom}
        </div>
        <Nav ChangePage={props.ChangePage}/>
    </header>
}
